import lombok.*;

import java.time.LocalDate;

@Setter
@Getter
@RequiredArgsConstructor
@ToString
@EqualsAndHashCode
@Builder
public class Book {

  private final String title;
  private final String author;
  private final LocalDate publishDate;
  private @With final int pages;

}
