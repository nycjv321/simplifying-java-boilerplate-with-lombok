
import lombok.val;

import java.time.LocalDate;

public class LombokDemo {
  public static void main(String[] args) {
    val book = Book
      .builder()
      .title("Domain-Driven Design")
      .author("Eric Evans")
      .publishDate(LocalDate.of(2003, 8, 1))
      .pages(528)
      .build();
    
    System.out.println(book);

  }
}
